# AndresRocco's build of dwm

Just the config I use on dwm.

DWM's official website:
https://dwm.suckless.org/

Patches used are:
+ Centered window name
+ Noborder
+ Pertag
+ Hide vacant tags
+ Vanity gaps

Could not get vanity gaps to apply as a patch so I had to manually add it into the source code.

## Installation 

```
git clone https://github.com/AndresRocco/dwm
cd dwm
sudo make clean install
```

